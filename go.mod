module allprotobuff

go 1.13

require (
	github.com/gin-gonic/gin v1.6.2
	github.com/golang/protobuf v1.4.0
	google.golang.org/grpc v1.29.1
	google.golang.org/protobuf v1.21.0
)
