from flask import Flask,jsonify,make_response

app = Flask(__name__)

import grpc
import service_pb2
import service_pb2_grpc

app = Flask(__name__)

@app.route('/')
def index():
    return jsonify({
          "result": "hi sobat",
      })

@app.route('/add/<a_request>/<b_request>')
def add(a_request,b_request):
    channel = grpc.insecure_channel('localhost:4040')
    stub = service_pb2_grpc.AddServiceStub(channel)
    request = service_pb2.Request(a=int(a_request),b=int(b_request))
    response = stub.Add(request)
    return jsonify({
          "result": response.result,
      })

@app.route('/multiply/<a_request>/<b_request>')
def multiply(a_request,b_request):
    channel = grpc.insecure_channel('localhost:4040')
    stub = service_pb2_grpc.AddServiceStub(channel)
    request = service_pb2.Request(a=int(a_request),b=int(b_request))
    response = stub.Multiply(request)
    return jsonify({
          "result": response.result,
      })

if __name__ == '__main__':
    app.run()